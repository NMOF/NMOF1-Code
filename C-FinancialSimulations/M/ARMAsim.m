function [S,r,e] = ARMAsim(mu,phi,theta,sigma,T,Tb)
% ARMAsim.m  -- version 2011-01-06

% -- prepare parameters
if nargin < 6, Tb = 0; end
phi = phi(:);        theta = theta(:);
p   = length(phi);   q     = length(theta);
T_compl = T + Tb + max(p,q);
lag_p = 1:p;
lag_q = 1:q;

% -- initialise vectors
r = ones(T_compl,1)  * mu;
e = randn(T_compl,1) * sigma;

% -- simulate returns
for t = (max(p,q)+1) : T_compl
   r(t) = mu + r(t-lag_p)' * phi + e(t-lag_q)' * theta + e(t);
end

% -- discard initial values & compute prices
e(1:(T_compl-T)) = [];
r(1:(T_compl-T)) = [];
S = exp(cumsum(r));