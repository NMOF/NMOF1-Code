% ODE1b.m  -- version 1997-12-09
f7 = 1;
x0 = 2;  y0 = 1;  % Initial condition
xN = 30; yN = 0;  % Final condition
N  = 30;          % Number of points
dx = (xN - x0)/N;
x  = linspace(x0,xN,N+1);
y(f7+0) = y0;  
x(f7+0) = x0;
for i = 1:N
    c(i) = x(f7+i)/(2*dx);
end
A=spdiags([[-c(2:N) NaN]' ones(N,1) [NaN c(1:N-1)]'],-1:1,N,N);
b = [c(1)*y0  zeros(1,N-2) -c(N)*yN]';
y(f7+(1:N)) = A\b;
