function C = Crossover(GA,P1)
% Crossover.m  -- version 2007-02-12
p = unidrnd(GA.nP1,2,1); % Select parents
a = p(1);   b = p(2);
k = unidrnd(GA.nC - 1) + 1;
C = [P1.C{a}(1:k-1),P1.C{b}(k:GA.nC)];
