function [F,z] = GAShekel(x,Data)
% GASheckel.m -- version 2009-11-07
d = Data.d;
n = length(x);
y = zeros(1,d);
for i = 1:d
    y(i) = bin2dec(x( 1+(i-1)*n/d : i*n/d ));
end
% written for domain (0,10) for all variables (Data.int !!)
y = y*10 /(2^(n/d) - 1);
if any(y>10) || any(y<0) ,error('values outside domain'),end
F = Shekel(y,Data); 
if nargout == 2, z = y; end