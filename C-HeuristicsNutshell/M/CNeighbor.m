function x1 = CNeighbor(x0,Data,TA)
% CNeighbor.m  -- version 2011-01-08
x1 = x0;
j = unidrnd(Data.d);   % Randomly select element of x0
x1(j) = x1(j) + randn * TA.frac;
% x1(j) = x1(j) + (4*rand - 2) * TA.frac;
% Check domain constraints
x1(j) = min(Data.int(j,2), max(Data.int(j,1),x1(j)) ); 
