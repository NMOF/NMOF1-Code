function output = GAH(GA,OF,Data)
% GAH.m -- version 2009-11-07
P = StartingPop(GA,OF,Data);
S = zeros(GA.nG,1); Fbest = realmax;
wbh = waitbar(0,[int2str(GA.nG),' generations ...']); tic
for r = 1:GA.nG
    P1 = MatingPool(GA,P);
    for i = 1:GA.nP2
        P2.C{i} = Crossover(GA,P1);
        P2.C{i} = Mutate(GA,P2.C{i});
    end
    P = Survive(GA,P1,P2,OF,Data);
    S(r) = P.F(1);
    if P.F(1) < Fbest, Fbest = P.F(1); xbest = P.C(1); end
    if all(~diff(P.F)), output.ng = r; break, end
    if ~rem(r,5),waitbar(r/GA.nG,wbh);end 
end,close(wbh),fprintf(' GA done (%i sec)\n',fix(toc))
output.P  = P;  output.S  = S;  output.ng = r;
output.Fbest = Fbest; output.xbest = xbest;
