function Data = StartingSol(OF,Data)
% StartingSol.m -- version 2011-08-13
for i = 1:Data.d
   Data.xs(i)=Data.int(i,1)+(Data.int(i,2)-Data.int(i,1))*rand;
end
Data.Fs = feval(OF,Data.xs,Data);
