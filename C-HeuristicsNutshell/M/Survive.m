function P = Survive(GA,P1,P2,OF,Data)
% Survive.m  -- version 2007-02-15
P2.F = zeros(1,GA.nP2);
for i = 1:GA.nP2
    P2.F(i) = feval(OF,P2.C{i},Data); % Fitness of children
end
F = [P1.F,P2.F];
[ignore,I] = sort(F);
for i = 1:GA.nP
    j = I(i);
    if j<= GA.nP1
        P.C{i} = P1.C{j}; % Surviving parents
        P.F(i) = P1.F(j);
    else
        P.C{i} = P2.C{j - GA.nP1}; % Suriving children
        P.F(i) = P2.F(j - GA.nP1);
    end
end
% Dying population
k = 0;
for i = GA.nP+1:length(F)
    k = k + 1;
    j = I(i);
    if j<= GA.nP1
        P.DC{k} = P1.C{j}; % Surviving parents
        P.DF(k) = P1.F(j);
    else
        P.DC{k} = P2.C{j - GA.nP1}; % Suriving children
        P.DF(k) = P2.F(j - GA.nP1);
    end
end
