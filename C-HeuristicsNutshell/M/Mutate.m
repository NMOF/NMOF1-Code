function C = Mutate(GA,C)
% Mutate.m  -- version 2007-02-11
if rand < GA.pM
    % Child undergoes mutation
    j = unidrnd(GA.nC); % Select chromosome
    if strcmp(C(j),'0')
        C(j) = '1';
    else
        C(j) = '0';
    end
end
