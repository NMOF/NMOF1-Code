function x1 = DNeighbor(x0,Data,ignore)
% DNeighbor.m  -- version  2011-01-09
x1 = x0;
j = unidrnd(Data.nc); % Select bit
i = unidrnd(Data.nc); % Select bit
u = rand;
if u < 1/3
    x1(j) = x0(i); x1(i) = x0(j);
elseif u < 2/3
    if strcmp(x1(i),'0'), x1(i) = '1'; else x1(i) = '0'; end
else
    if strcmp(x1(j),'0'), x1(j) = '1'; else x1(j) = '0'; end
end


