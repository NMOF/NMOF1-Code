function output = TAH(TA,OF,NF,Data)
% TAH.m -- version 2009-11-09
F0 = Data.Fs;  x0 = Data.xs;
output.FF = repmat(NaN,TA.Rounds*TA.Steps,1);
Fbest = F0;   xbest = x0;  output.FF(1) = F0; 
iup = 1; tic
wbh = waitbar(0,[int2str(TA.Rounds),' rounds with ',int2str(TA.Steps),' steps ...']);
for iround = 1:TA.Rounds
    for istep = 1:TA.Steps
        x1 = feval(NF,x0,Data,TA);
        F1 = feval(OF,x1,Data);
        if F1 <= F0 + TA.th(iround)
            iup = iup + 1; 
            output.FF(iup) = F1; 
            F0 = F1;  
            x0 = x1;
            if F1 < Fbest, Fbest = F1; xbest = x1; end
        end
    end  % end steps
    output.uprounds(iround) = iup;
    waitbar(iround/TA.Rounds,wbh);
end, close(wbh), fprintf(' TA done (%i sec)\n', fix(toc));
output.Fbest = Fbest;  output.xbest = xbest;  output.iup = iup;      

