clear variables, close all hidden
%rand('state',12345)
%OF = 'GAShekel'; Data = struct('int',[0 10; 0 10],'m',10,'d',2);

% goGA.m -- version 2009-11-07
OF = 'BitString';  Data.X = 2^50 + 2^25 +2^20 + 2^5 + 2^0;
GA = struct('nG',500,'pM',0.2,'nC',52,'nP',200,'nP1',100,...
            'nP2',200,'Restarts',10);

for r = 1:GA.Restarts
    output = GAH(GA,OF,Data);
    Sol(r) = output.Fbest;
    X{r} = output.xbest;
    plotGA(GA,output);
end
[Sbest,i] = min(Sol);
fprintf('\n Sol = %6.3f  ',Sbest); disp(X{i});

figure(2), subplot(211), H = cdfplot(Sol); 
xlabel(''); ylabel(''); title('');
set(H,'Color',[.8 .8 .8],'LineWidth',3), set(gca,'FontSize',10)

% print -depsc ..\Figs\M-GA-cdf00.eps
