function output = thSequence(TA,OF,NF,Data)
% thSequence.m -- version 2011-08-13
F0 = Data.Fs;  x0 = Data.xs;
nND = min(TA.Rounds*TA.Steps,5000);
ND0 = zeros(1,nND);
wbh = waitbar(0,['Exploring ',int2str(nND),' neighbor solutions ...']);
for s = 1:nND
    x1 = feval(NF,x0,Data,TA);
    F1 = feval(OF,x1,Data);
    ND0(s) = F1 - F0;
    F0 = F1;  x0 = x1;
    if ~rem(s,fix(nND/50)), waitbar(s/nND,wbh); end
end, close(wbh);
ntrim = max(fix(length(nND)*TA.ptrim),10);
ND = sort(ND0);
ip = find(ND > 0);
output.ND = ND(ip:end-ntrim);
output.th = quantile(output.ND,TA.Percentiles);
%
figure(1), subplot(211)
H = cdfplot(output.ND); xlim([0 output.th(1)]);
hold on; xlabel(''); ylabel(''); title('');
set(H,'Color',.7*[1 1 1],'LineWidth',2), set(gca,'FontSize',10)
plot(fliplr(output.th),fliplr(TA.Percentiles),'ko',...
     'MarkerSize',6);
set(gca,'ytick',fliplr(TA.Percentiles));
set(gca,'xtick',fliplr(output.th));

