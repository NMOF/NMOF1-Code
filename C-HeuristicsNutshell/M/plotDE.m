function plotDE(DE,output)
% plotDE.m  -- version 2012-04-11
clf(gcf), figure(1), subplot(212)
semilogy(1:DE.nG,output.Fbv,'ko','MarkerSize',8,...
         'MarkerFaceColor',.7*[1 1 1]);
set(gca,'FontSize',10); grid on; xlabel('Generations');
