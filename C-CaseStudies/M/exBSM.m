% exBMS.m -- version 2010-12-08
S     = 100;
X     = 100;
tau   = 1/2;
r     = 0.03;
q     = 0.05;
v     = 0.2^2;

% MC parameters
M = 1;
N = 100000;

%% analytic solution
tic, call = callBSM(S,X,tau,r,q,v); t=toc;
fprintf('The analytic solution.\ncall price %6.2f.   It took %6.2f seconds.\n', call,t)

%% MC
tic, [call, payoff] = callBSMMC(S,X,tau,r,q,v,M,N); t=toc;
SE = std(payoff)/sqrt(N);
fprintf('\nMC 1 (vectorized).\ncall price %6.2f.   lower band %6.2f.   upper band %6.2f.   width of CI %6.2f.   It took %6.2f seconds.\n',...
    call, -2*SE+call, 2*SE+call, 4*SE, t)


%% pathwise
tic, [call, Q] = callBSMMC2(S,X,tau,r,q,v,M,N); t=toc;
SE = sqrt(Q/N)/sqrt(N);
fprintf('\nMC 2 (loop).\ncall price %6.2f.   lower band %6.2f.   upper band %6.2f.   width of CI %6.2f.   It took %6.2f seconds.\n',...
    call, -2*SE+call, 2*SE+call, 4*SE, t)


%% variance reduction: antithetic
tic, [call, Q] = callBSMMC3(S,X,tau,r,q,v,M,N); t=toc;
SE = sqrt(Q/N)/sqrt(N);
fprintf('\nMC 3 (loop), antithetic variates.\ncall price %6.2f.   lower band %6.2f.   upper band %6.2f.   width of CI %6.2f.   It took %6.2f seconds.\n',...
    call, -2*SE+call, 2*SE+call, 4*SE, t)


%% variance reduction: antithetic
tic, [call, payoff] = callBSMMC4(S,X,tau,r,q,v,M,N); t=toc;
SE = std(payoff)/sqrt(N);
fprintf('\nMC 4 (vectorized), antithetic variates.\ncall price %6.2f.   lower band %6.2f.   upper band %6.2f.   width of CI %6.2f.   It took %6.2f seconds.\n',...
    call, -2*SE+call, 2*SE+call, 4*SE, t)


%% variance reduction: control variate
tic, [call, Q] = callBSMMC5(S,X,tau,r,q,v,M,N); t=toc;
SE = sqrt(Q/N)/sqrt(N);
fprintf('\nMC 5 (loop), control variate.\ncall price %6.2f.   lower band %6.2f.   upper band %6.2f.   width of CI %6.2f.   It took %6.2f seconds.\n',...
    call, -2*SE+call, 2*SE+call, 4*SE, t)